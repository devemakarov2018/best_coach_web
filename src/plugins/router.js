import Vue from 'vue'
import VueRouter from 'vue-router'

import ExercisesAddEdit from '../components/ExercisesAddEdit'
import ExercisesIndex from '../components/ExercisesIndex'
import ExercisesView from '../components/ExercisesView'
import GradesIndex from '../components/GradesIndex'
import Login from '../components/Login'
import NotFound from '../components/NotFound'
import StudentsIndex from '../components/StudentsIndex'
import StudentsView from '../components/StudentsView'
import auth from '../services/auth'
import TheoryIndex from '../components/TheoryIndex'
import StandardIndex from '../components/StandardIndex'
import NewsIndex from '../components/NewsIndex'
import TheoryAddEdit from '../components/TheoryAddEdit'
import NewsAddEdit from '../components/NewsAddEdit'
import TheoryView from '../components/TheoryView'
import Help from '../components/Help'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'root',
    component: Login
  },
  {
    path: '/login',
    name: 'login',
    component: Login
  },
  {
    path: '/grades',
    name: 'grades',
    component: GradesIndex
  },
  {
    path: '/students',
    name: 'studentsIndex',
    component: StudentsIndex
  },
  {
    path: '/students/view/:id',
    name: 'studentsView',
    component: StudentsView
  },
  {
    path: '/exercises',
    name: 'exercisesIndex',
    component: ExercisesIndex
  },
  {
    path: '/exercises/edit/:id',
    name: 'exercisesEdit',
    component: ExercisesAddEdit
  },
  {
    path: '/exercises/add',
    name: 'exercisesAdd',
    component: ExercisesAddEdit
  },
  {
    path: '/exercises/view/:id',
    name: 'exercisesView',
    component: ExercisesView
  },
  {
    path: '/theory',
    name: 'theoryIndex',
    component: TheoryIndex
  },
  {
    path: '/theory/edit/:id',
    name: 'theoryEdit',
    component: TheoryAddEdit
  },
  {
    path: '/theory/add',
    name: 'theoryAdd',
    component: TheoryAddEdit
  },
  {
    path: '/theory/view/:id',
    name: 'theoryView',
    component: TheoryView
  },
  {
    path: '/standards',
    name: 'standardIndex',
    component: StandardIndex
  },
  {
    path: '/news',
    name: 'newsIndex',
    component: NewsIndex
  },
  {
    path: '/news/edit/:id',
    name: 'newsEdit',
    component: NewsAddEdit
  },
  {
    path: '/news/add',
    name: 'newsAdd',
    component: NewsAddEdit
  },
  {
    path: '/help',
    name: 'help',
    component: Help
  },
  {
    path: '*',
    'name': 'notFound',
    component: NotFound
  },
]

const router = new VueRouter({
  routes: routes
})

router.beforeEach((to, from, next) => {

  const isLoggined = auth.user();

  if (isLoggined && to.name === 'root') {
    next({name: 'studentsIndex'})
  }
  else if (isLoggined || to.name === 'login') {
    next();
  } else {
    next({name: 'login'})
  }
})

export default router