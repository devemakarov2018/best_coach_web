import axios from 'axios'
import auth from './auth'

let token = '';
let request = null;


function init() {
  token = auth.user() && auth.user().token
    ? auth.user().token
    : '';
  request = axios.create({
    baseURL: `${process.env.VUE_APP_API_URL}`,
    headers: {'Authorization': `Bearer ${token}`}
  })
}

init();

const api = {
  refresh: () => init(),
  grades: {
    getAll: function () {
      return new Promise((resolve, reject) => {
        request.get('/grades')
          .then(res => resolve(res.data.result.grades))
          .catch(err => reject(err))
      })
    },
    add: function (grade) {
      return new Promise((resolve, reject) => {
        request.post('/grades/add', grade)
          .then(res => resolve(res.data.result.grade))
          .catch(err => reject(err))
      })
    },
    edit: function (grade) {
      return new Promise((resolve, reject) => {
        request.put(`/grades/edit/${grade.id}`, grade)
          .then(res => resolve(res.data.result.grade))
          .catch(err => reject(err))
      })
    },
    delete: function (grade) {
      return new Promise((resolve, reject) => {
        request.delete(`/grades/delete/${grade.id}`)
          .then(res => resolve(res.data.result.grade))
          .catch(err => reject(err))
      })
    }
  },
  students: {
    getAll: function () {
      return new Promise((resolve, reject) => {
        request.get('/students')
          .then(res => resolve(res.data.result.students))
          .catch(err => reject(err))
      })
    },
    get: function(id) {
      return new Promise((resolve, reject) => {
        request.get(`/students/view/${id}`)
          .then(res => resolve(res.data.result.student))
          .catch(err => reject(err))
      })
    },
    add: function (student) {
      return new Promise((resolve, reject) => {
        request.post('/students/add', student)
          .then(res => resolve(res.data.result.student))
          .catch(err => reject(err))
      })
    },
    edit: function (student) {
      return new Promise((resolve, reject) => {
        request.put(`/students/edit/${student.id}`, student)
          .then(res => resolve(res.data.result.student))
          .catch(err => reject(err))
      })
    },
    delete: function (student) {
      return new Promise((resolve, reject) => {
        request.delete(`/students/delete/${student.id}`)
          .then(res => resolve(res.data.result.student))
          .catch(err => reject(err))
      })
    },
    changeGrade: function(students, grade) {
      return new Promise((resolve, reject) => {
        request.post('/students/change-grade', {
          students: students.map(s => s.id),
          grade: grade.id
        })
          .then(res => resolve())
          .catch(err => reject(err))
      })
    },
    exportExercises: function(studentId, mode) {
      return new Promise((resolve, reject) => {
        request.get(`/export/exercises/${studentId}/${mode}`, {
          responseType: 'blob'
        })
          .then(res => {
            const url = window.URL.createObjectURL(new Blob([res.data]));
            const link = document.createElement('a');
            link.href = url;
            link.setAttribute('download', 'exercises.txt');
            document.body.appendChild(link);
            link.click();
            resolve()
          })
          .catch(err => reject(err))
      })
    }
  },
  exercises: {
    getAll: function () {
      return new Promise((resolve, reject) => {
        request.get('/exercises')
          .then(res => resolve(res.data.result.exercises))
          .catch(err => reject(err))
      })
    },
    get: function (id) {
      return new Promise((resolve, reject) => {
        request.get(`/exercises/view/${id}`)
          .then(res => {
            resolve(res.data.result.exercise)
          })
          .catch(err => reject(err))
      })
    },
    add: function(exercise) {
      return new Promise((resolve, reject) => {
        delete exercise.students
        request.post('/exercises/add', {
          Exercise: exercise,
          ExerciseComponents: exercise.exercise_components,
          ExerciseCategories: exercise.exercise_categories.map(category => category.id)
        })
          .then(res => resolve(res.data.result.exercise))
          .catch(err => reject(err))
      })
    },
    edit: function (exercise) {
      return new Promise((resolve, reject) => {
        delete exercise.students
        request.put(`/exercises/edit/${exercise.id}`, {
          Exercise: exercise,
          ExerciseComponents: exercise.exercise_components,
          ExerciseCategories: exercise.exercise_categories.map(category => category.id)
        })
          .then(res => resolve(res.data.result.exercise))
          .catch(err => reject(err))
      })
    },
    delete: function (exercise) {
      return new Promise((resolve, reject) => {
        request.delete(`/exercises/delete/${exercise.id}`)
          .then(res => resolve(res.data.result.exercise))
          .catch(err => reject(err))
      })
    }
  },
  exercisesStudents: {
    add: function(exercises, students, period) {
      return new Promise((resolve, reject) => {
        request.post('/exercises-students/add', {
          exercises: exercises.map(e => e.id),
          students: students.map(s => s.id),
          period: period
        })
          .then(() => resolve())
          .catch(err => reject(err))
      })
    }
  },
  images: {
    upload: function(file) {
      return new Promise((resolve, reject) => {
        let formData = new FormData()
        formData.append('image', file)
        request.post('/images/upload',
          formData, {
          headers: {'Content-Type': 'multipart/form-data'}
        })
          .then(res => resolve(res.data.result.path))
          .catch(err => reject(err))
      })
    }
  },
  exerciseCategories: {
    getAll: function() {
      return new Promise((resolve, reject) => {
        request.get('/exercise-categories')
          .then(res => resolve(res.data.result.categories))
          .catch(err => reject(err))
      })
    },
    add: function(category) {
      return new Promise((resolve, reject) => {
        request.post('/exercise-categories/add', category)
          .then(() => resolve())
          .catch(err => reject(err))
      })
    }
  },
  screens: {
    get: function (name) {
      return new Promise((resolve, reject) => {
        request.get(`/screens/view/${name}`)
          .then(res => resolve(res.data.result.screen))
          .catch(err => reject(err))
      })
    },
    edit: function(screen) {
      return new Promise((resolve, reject) => {
        delete screen.screen
        request.put(`/screens/edit/${screen.id}`, screen)
          .then(res => resolve(res.data.result.screen))
          .catch(err => reject(err))
      })
    }
  },
  theory: {
    getAll: function () {
      return new Promise((resolve, reject) => {
        request.get('/theory-details')
          .then(res => resolve(res.data.result.theoryDetails))
          .catch(err => reject(err))
      })
    },
    get: function (id) {
      return new Promise((resolve, reject) => {
        request.get(`/theory-details/view/${id}`)
          .then(res => {
            resolve(res.data.result.theoryDetail)
          })
          .catch(err => reject(err))
      })
    },
    add: function(theory) {
      return new Promise((resolve, reject) => {
        delete theory.students
        request.post('/theory-details/add', {
          TheoryDetails: theory,
          TheoryDetailsComponents: theory.theory_details_components,
          TheoryDetailsCategories: theory.theory_details_categories.map(category => category.id)
        })
          .then(res => resolve(res.data.result.theoryDetail))
          .catch(err => reject(err))
      })
    },
    edit: function (theory) {
      return new Promise((resolve, reject) => {
        delete theory.students
        request.put(`/theory-details/edit/${theory.id}`, {
          TheoryDetails: theory,
          TheoryDetailsComponents: theory.theory_details_components,
          TheoryDetailsCategories: theory.theory_details_categories.map(category => category.id)
        })
          .then(res => resolve(res.data.result.theoryDetail))
          .catch(err => reject(err))
      })
    },
    delete: function (theory) {
      return new Promise((resolve, reject) => {
        request.delete(`/theory-details/delete/${theory.id}`)
          .then(res => resolve(res.data.result.theoryDetail))
          .catch(err => reject(err))
      })
    }
  },
  theoryCategories: {
    getAll: function() {
      return new Promise((resolve, reject) => {
        request.get('/theory-details-categories')
          .then(res => resolve(res.data.result.categories))
          .catch(err => reject(err))
      })
    },
    add: function(category) {
      return new Promise((resolve, reject) => {
        request.post('/theory-details-categories/add', category)
          .then(() => resolve())
          .catch(err => reject(err))
      })
    }
  },
  news: {
    getAll: function () {
      return new Promise((resolve, reject) => {
        request.get('/news')
          .then(res => resolve(res.data.result.news))
          .catch(err => reject(err))
      })
    },
    get: function (id) {
      return new Promise((resolve, reject) => {
        request.get(`/news/view/${id}`)
          .then(res => {
            delete res.data.result.news.students
            resolve(res.data.result.news)
          })
          .catch(err => reject(err))
      })
    },
    add: function(news) {
      return new Promise((resolve, reject) => {
        request.post('/news/add', {
          News: news,
          NewsComponents: news.news_components
        })
          .then(res => resolve(res.data.result.news))
          .catch(err => reject(err))
      })
    },
    edit: function (news) {
      return new Promise((resolve, reject) => {
        request.put(`/news/edit/${news.id}`, {
          News: news,
          NewsComponents: news.news_components
        })
          .then(res => resolve(res.data.result.news))
          .catch(err => reject(err))
      })
    },
    delete: function (news) {
      return new Promise((resolve, reject) => {
        request.delete(`/news/delete/${news.id}`)
          .then(res => resolve(res.data.result.news))
          .catch(err => reject(err))
      })
    }
  },
  theoryDetailsStudents: {
    add: function(theoryDetails, students) {
      return new Promise((resolve, reject) => {
        request.post('/theory-details-students/add', {
          theoryDetails: theoryDetails.map(t => t.id),
          students: students.map(s => s.id),
        })
          .then(() => resolve())
          .catch(err => reject(err))
      })
    }
  },
}

export default api