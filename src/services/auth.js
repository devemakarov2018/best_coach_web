import axios from 'axios'

const api = axios.create({
  baseURL: `${process.env.VUE_APP_API_URL}/auth`
})

const auth = {
  login: function (user) {
    return new Promise((resolve, reject) => {
      api.post('/login-teacher', user)
        .then(res => {
          localStorage.clear()
          localStorage.setItem('token', res.data.result.token)
          localStorage.setItem('user', JSON.stringify(res.data.result.user))
          resolve()
        })
        .catch(err => reject(err))
    })
  },
  user: function () {
    let token = localStorage.getItem('token')
    let user = localStorage.getItem('user')
    if (!token || !user) {
      return null
    }
    return {
      token: token,
      user: JSON.parse(user)
    }
  },
  logout: function () {
    localStorage.clear();
  }
}

export default auth